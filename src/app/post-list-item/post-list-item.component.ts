import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() loveIts: number;
  @Input() created_at: Date;

  constructor() { }

  loveit() {
    this.loveIts++;
  }

  dontloveit() {
    this.loveIts--;
  }

  getlove() {
    return this.loveIts > 0;
  }
  getnotlove() {
    return this.loveIts < 0;
  }
  getnew() {
    return this.loveIts === 0;
  }
  ngOnInit() {
  }

}
