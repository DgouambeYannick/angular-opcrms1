import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'exo1';

  p1 = new Post('Mon premier Post', 'Mon premier Post Mon premier Post Mon premier Post Mon premier Post Mon premier Post Mon premier ', 5);
  p2 = new Post('Mon deuxieme Post', 'Mon deuxieme Post Mon deuxieme Post Mon deuxieme Post Mon deuxieme Post Mon deuxieme Post Mont', -2);
  p3 = new Post('Mon troisieme Post', 'Mon troisieme Post Mon troisieme Post Mon troisieme Post Mon troisieme Post Mon troisieme Post', 2);
  p4 = new Post('Nouveau Post', 'Mon nouveau Post Mon nouveau Post Mon nouveau Post Mon nouveau Post Mon nouveau Post', 0);

  public posts = [this.p1, this.p2, this.p3, this.p4];

}

export class Post {
  title: string;
  content: string;
  loveIts: number;
  created_at: Date;
  constructor(title: string, content: string, love: number) {
    this.title = title;
    this.content = content;
    this.loveIts = love;
    this.created_at = new Date();
  }
}

